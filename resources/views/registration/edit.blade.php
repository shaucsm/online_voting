<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>First Private Bank | Registration</title>
        
        <link rel="icon" type="image/x-icon" href="{{ URL::asset('img/favicon.ico') }}"/>
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.min.css') }}"/>
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/dataTables.bootstrap4.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/styles.css') }}" media="display"/>
        <style>
            span.require{color:red;}
        </style>
    </head>
    <body id="page-top">
        <!-- Navigation-->
		<nav class="navbar navbar-expand-lg navbar-light bg-light static-top">
			<div class="container">
				<a class="navbar-brand" href="#">
					<img src="{{ URL::asset('img/logo.png') }}" alt="Logo" height="56px">
					<lable class="pl-2">First Private Bank</lable>
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarResponsive">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item active">
							<a class="nav-link " href="{% url 'patient' 0 0 %}" class="">Registration</a>                  
						</li>
					</ul>
				</div>
			</div>
        </nav>
        <!-- Content -->
        <section class="content-section mb-3" id="services">
            <div class="container">
                <form name="RegistrationForm" method="POST" action="">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="name" class="require">Name <span class="require">*</span></label>
                            <input readonly type="text" name="name" class="form-control" id="name" value="{{ $registration['name'] }}" required>
                        </div>
                        
                        <div class="form-group col-md-6">
                            <label for="firstname">First Name <span class="require">*</span></label>
                            <input readonly type="text" name="firstname" class="form-control" id="firstname" value="{{ $registration['firstname'] }}" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="lastname">Last Name</label>
                            <input readonly type="text" name="lastname" class="form-control" id="lastname" value="{{ $registration['lastname'] }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="fathername">Father Name <span class="require">*</span></label>
                            <input readonly type="text" name="fathername" class="form-control" id="fathername" value="{{ $registration['fathername'] }}">
                            @error('fathername')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="address">Address <span class="require">*</span></label>
                        <input readonly type="text" name="address" class="form-control" id="address" value="{{ $registration['address'] }}">
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="m_mobile">Mobile Phone no. (Myanmar) <span class="require">*</span></label>
                            <input readonly type="text" name="m_mobile" class="form-control" id="m_mobile" value="{{ $registration['m_mobile'] }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="f_mobile">Mobile Phone no. (Oversea)</label>
                            <input readonly type="text" name="f_mobile" class="form-control" id="f_mobile" value="{{ $registration['f_mobile'] }}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="m_lineno">Line Telephone no. (Myanmar)</label>
                            <input readonly type="text" name="m_lineno" class="form-control" id="m_lineno" value="{{ $registration['m_lineno'] }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="f_lineno">Line Telephone no. (Oversea)</label>
                            <input readonly type="text" name="f_lineno" class="form-control" id="f_lineno" value="{{ $registration['f_lineno'] }}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="id_no">ID Number (Special Number/security Number) <span class="require">*</span></label>
                            <input readonly type="text" name="id_no" class="form-control" id="id_no" value="{{ $registration['id_no'] }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="share">Number of Shares <span class="require">*</span></label>
                            <input readonly type="text" name="share" class="form-control" id="share" value="{{ $registration['share'] }}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-primary btn-block">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        
        <!-- For Bootstrap -->
        <script src="{{ URL::asset('js/jquery-3.5.1.min.js') }}"></script>
        <script src="{{ URL::asset('js/popper.min.js') }}"></script>
        
        <!-- Bootstrap core JS-->
        <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>

        <script src="{{ URL::asset('js/jquery.dataTables.js') }}"></script>
        <script src="{{ URL::asset('js/dataTables.bootstrap4.min.js') }}"></script>
    </body>
</html>
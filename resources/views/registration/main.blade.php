<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>First Private Bank | Registration</title>

  <!-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">
  <link href="{{ asset('css/animate.css') }}" rel="stylesheet"> -->
  <link rel="icon" type="image/x-icon" href="{{ URL::asset('img/favicon.ico') }}"/>
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.min.css') }}"/>
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/dataTables.bootstrap4.min.css') }}">
  
  @yield('css')

</head>

<body>
  <div class="container" id="container">
    @yield('content')
  </div>

  <!-- Mainly scripts -->
  <!-- <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}"></script> -->

  @yield('js')

</body>

</html>

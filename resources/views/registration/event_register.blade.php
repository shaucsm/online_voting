@extends('registration.main')

@section('content')
<div class="row mt-3">
  <div class="col-md-7 pl-0">
    <div class="w-100">
      <img style="width:100%" src="{{ URL::asset('img/agenda.png') }}" alt="Agenda">
    </div>
    <div id="left-content">
    </div>
  </div>
  <div class="col-md-5 right-side">
      <div class="form-container">
        <h4 class="form-text text-center py-3">၂၉ကြိမ်မြောက်အထွေထွေအစည်းအဝေးမှတ်ပုံတင်ခြင်း</h4>
        <form action="{{route('registration_post')}}" method="POST" id="store">
          @csrf
          <div class="form-group col-12">
            <label for="name" class="form-text">Name <span class="require">*</span></label>
            <input type="text" name="name" class="form-control" id="name" value="" required>
          </div>

          <div class="form-group col-md-12">
            <label for="firstname" class="form-text">First Name <span class="require">*</span></label>
            <input type="text" name="firstname" class="form-control" id="firstname" required>
          </div>

          <div class="form-group col-md-12">
            <label for="lastname" class="form-text">Last Name</label>
            <input type="text" name="lastname" class="form-control" id="lastname" >
          </div>

          <div class="form-group col-md-12">
            <label for="fathername" class="form-text">Father Name <span class="require">*</span></label>
            <input type="text" name="fathername" class="form-control" id="fathername" required>
          </div>
                    
          <div class="form-group col-md-12">
              <label for="address" class="form-text">Address <span class="require">*</span></label>
              <input type="text" name="address" class="form-control" id="address" required>
          </div>
          
          <div class="form-group col-md-12">
              <label for="m_mobile" class="form-text">Mobile Phone no. (Myanmar) <span class="require">*</span></label>
              <input type="text" name="m_mobile" class="form-control" id="m_mobile" required>
          </div>
          <div class="form-group col-md-12">
              <label for="f_mobile" class="form-text">Mobile Phone no. (Oversea)</label>
              <input type="text" name="f_mobile" class="form-control" id="f_mobile" >
          </div>
          
          <div class="form-group col-md-12">
              <label for="m_lineno" class="form-text">Line Telephone no. (Myanmar)</label>
              <input type="text" name="m_lineno" class="form-control" id="m_lineno" >
          </div>
          <div class="form-group col-md-12">
              <label for="f_lineno" class="form-text">Line Telephone no. (Oversea)</label>
              <input type="text" name="f_lineno" class="form-control" id="f_lineno" >
          </div>
          
          <div class="form-group col-md-12">
              <label for="id_no" class="form-text">ID Number (Special Number/security Number) <span class="require">*</span></label>
              <input type="text" name="id_no" class="form-control" id="id_no" required>
          </div>
          <div class="form-group col-md-12">
              <label for="share" class="form-text">Number of Shares <span class="require">*</span></label>
              <input type="text" name="share" class="form-control" id="share" required>
          </div>          
          @if ( ! \Session::has('mail_exists'))   
          <div class="form-group col-12">
            <button type="submit" form="store" class="btn btn-block btn-primary btn-invert form-text"> Register now </button>
          </div>
          @endif
        </form>
    </div>
  </div> 
  <!-- Column -->
</div>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/reg-theme.css') }}"/>

<style>

.form-control:focus{
  box-shadow : none;
}

.form-group input{
    background-color: #fff;
  }

  .form-container {
    background-color:#359cd6;
    padding : .5rem;
  }

  .custom-primary{
    color:#359cd6;
  }

  .form-text {
    font-weight: 700;
    color:#fff;
  }

  .error-custom {
    font-weight: 300;
    color:#fff;
  }

  a.error-custom{
    font-weight: 500;
    text-decoration: underline;
  }

  a.error-custom:hover{
    font-weight: 500;
    color: #f0f0f0;
    text-decoration: underline;
  }

  .btn-invert {
    border: solid 3px #fff;
    background-color: transparent;
  }

  .btn-invert:hover {
    color:#359cd6;
    border: solid 3px #fff;
    background-color:#fff;
  }

  /* styles for left-side */

  #left-content h1,
  #left-content h2,
  #left-content h3,
  #left-content h4,
  #left-content h5,
  #left-content h6{
    color: #359cd6;
  }

  #left-content > figure{
    width:100%;
  }
  
/* table, table td {
  border: solid 1px rgb(90, 90, 90);
  border-top: solid 1px rgb(90, 90, 90) !important;
} */


</style>
@endsection

@section('js')
<script>
  $(document).ready(function () {
  // $("#left-content>figure>img").each(element => {
  //   console.log($(element).width() )
  // });
    $('table').addClass('table-striped table-bordered');

  })
</script>
@endsection

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Event by MSIS</title>
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Font-->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/opensans-font.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('fonts/line-awesome/css/line-awesome.min.css') }}">
	
	<!-- Main Style Css -->
	<link rel="stylesheet" href="{{ asset('css/style.css') }}"/>
	<link rel="stylesheet" href="{{ asset('css/reg-theme.css') }}"/>
</head>
<body class="form-v4">
	<div class="page-content">
		<div class="form-v4-content thank-form"  >
			<div class="form-left bg-white">

				<h1 style="margin-bottom:.2rem">Thank You Very Much
				</h1>
				<span style="font-weight:400"> You have sucessfully registered! We will send you Meeting link via email.  </span>

			</div>

		</div>
	</div>
	<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>
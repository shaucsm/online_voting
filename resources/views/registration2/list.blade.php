<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>First Private Bank | Registration</title>
        
        <link rel="icon" type="image/x-icon" href="{{ URL::asset('img/favicon.ico') }}"/>
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.min.css') }}"/>
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/dataTables.bootstrap4.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/styles.css') }}" media="display"/>
    </head>
    <body id="page-top">
        <!-- Navigation-->
		<nav class="navbar navbar-expand-lg navbar-light bg-light static-top">
			<div class="container">
				<a class="navbar-brand" href="#">
					<img src="{{ URL::asset('img/logo.png') }}" alt="Logo" height="56px">
					<lable class="pl-2">First Private Bank</lable>
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarResponsive">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item active">
							<a class="nav-link " href="{% url 'patient' 0 0 %}" class="">Registration</a>                  
						</li>
					</ul>
				</div>
			</div>
        </nav>
        @php print_r($registrations); @endphp
        <!-- Content -->
        <section class="content-section mb-3" id="services">
            <div class="container">
                <table class="table table-bordered" id="laravel_crud">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>FirstName</th>
                            <th>reg Code</th>
                            <th>Description</th>
                            <th>Created at</th>
                            <td colspan="2">Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($registrations as $reg)
                        <tr>
                            <td>{{ $reg->id }}</td>
                            <td>{{ $reg->name }}</td>
                            <td>{{ $reg->firstname }}</td>
                            <td>{{ $reg->lastname }}</td>
                            <td></td>
                            <td><a href="{{ route('registration.edit',$reg->id)}}" class="btn btn-primary">Edit</a></td>
                            <td>
                                <form action="{{ route('registration.destroy', $reg->id)}}" method="post">
                                {{ csrf_field() }}
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </section>
        
        <!-- For Bootstrap -->
        <script src="{{ URL::asset('js/jquery-3.5.1.min.js') }}"></script>
        <script src="{{ URL::asset('js/popper.min.js') }}"></script>
        
        <!-- Bootstrap core JS-->
        <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>

        <script src="{{ URL::asset('js/jquery.dataTables.js') }}"></script>
        <script src="{{ URL::asset('js/dataTables.bootstrap4.min.js') }}"></script>
    </body>
</html>
<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('login', 'AuthController@index');
Route::post('login', 'AuthController@login'); 
Route::get('logout', 'AuthController@logout');

//Route::get('registration', 'RegistrationController@index')->name('registration.index');
Route::get('/', 'RegistrationController@create')->name('registration.create');
Route::post('registration_post', 'RegistrationController@store')->name('registration_post');
Route::get('registration_success', 'RegistrationController@success')->name('registration_success');

Route::get('registration/{id}', 'RegistrationController@show')->name('registration.show');
Route::get('registration/{id}/edit', 'RegistrationController@edit')->name('registration.edit');
Route::put('registration/{id}', 'RegistrationController@update')->name('registration.update');

Route::delete('registration/{id}', 'RegistrationController@destroy')->name('registration.destroy');

Route::get('dashboard', 'AuthController@dashboard')->name('registration.store');

// GET           /registration                      index   registration.index
// GET           /registration/create               create  registration.create
// POST          /registration                      store   registration.store
// GET           /registration/{user}               show    registration.show
// GET           /registration/{user}/edit          edit    registration.edit
// PUT|PATCH     /registration/{user}               update  registration.update
// DELETE        /registration/{user}               destroy registration.destroy
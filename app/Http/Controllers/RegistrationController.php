<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator,Redirect,Response;
//Use App\User;
//use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Registration;

use Session;

class RegistrationController extends Controller {
    public function index() {
        $data['registrations'] = Registration::orderBy('id','desc')->paginate(10);
        return view('registration.list',$data);

        $registrations = Registration::all();
        print_r($registrations['name']);
        //return view('registration',compact('registrations'));
    }

    public function create() {
        return view('registration.event_register');
    }

    public function store(Request $request) {  
        // $request()->validate([
        //   'name' => 'required',
        //   'firstname' => 'required',
        //   //'email' => 'required|email|unique:users',
        //   //'password' => 'required|min:6',
        // ]);

        

        //$idno = $request->input('id_no');
        //echo Hash::make($idno);

        $registration = new Registration();

        $registration->name = encrypt($request->input('name')); 
        $registration->firstname = encrypt($request->input('firstname')); 
        $registration->lastname = encrypt($request->input('lastname')); 
        $registration->fathername = encrypt($request->input('fathername')); 
        $registration->address = encrypt($request->input('address')); 
        $registration->m_mobile = encrypt($request->input('m_mobile')); 
        $registration->f_mobile = encrypt($request->input('f_mobile')); 
        $registration->m_lineno = encrypt($request->input('m_lineno')); 
        $registration->f_lineno = encrypt($request->input('f_lineno')); 
        $registration->id_no = encrypt($request->input('id_no')); 
        $registration->share = encrypt($request->input('share')); 

        $registration->save();

        // $check = $this->create($data);
       
        return view('registration.thank');
        // return Redirect::to("dashboard")->withSuccess('Great! You have Successfully loggedin');
    }

    public function success() {
        return view('registration.success');
    }

    public function show(Request $request) {
    }

    public function edit($id) {   
        $where = array('id' => $id);
        //$data['registration'] = Registration::where($where)->first();
        $registration = Registration::where($where)->first();

        $registration->name = self::decrypt_value($registration->name); 
        $registration->firstname = self::decrypt_value($registration->firstname); 
        $registration->lastname = self::decrypt_value($registration->lastname); 
        $registration->fathername = self::decrypt_value($registration->fathername); 
        $registration->address = self::decrypt_value($registration->address); 
        $registration->m_mobile = self::decrypt_value($registration->m_mobile); 
        $registration->f_mobile = self::decrypt_value($registration->f_mobile); 
        $registration->m_lineno = self::decrypt_value($registration->m_lineno); 
        $registration->f_lineno = self::decrypt_value($registration->f_lineno); 
        $registration->id_no = self::decrypt_value($registration->id_no); 
        $registration->share = self::decrypt_value($registration->share); 

        return view('registration.edit', compact('registration'));
    }

    public function update(Request $request, $id) {
        $request->validate([
            'title' => 'required',
            'product_code' => 'required',
            'description' => 'required',
        ]);
        $update = ['title' => $request->title, 'description' => $request->description];
        
        Product::where('id',$id)->update($update);
        
        return Redirect::to('products')->with('success','Great! Product updated successfully');
    }

    public function destroy($id) {
        Product::where('id',$id)->delete();
        return Redirect::to('registration')->with('success','Deletetion success!');
    }

    private function decrypt_value($value) {
        $decrypted = "";
        //Exception for decryption thrown in facade
        try {
            $decrypted = decrypt($value);
        } catch (DecryptException $e) {
            
        }

        return $decrypted;
    }
}